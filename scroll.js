const slidingNewsletter = document.querySelector('.slide-in');

window.addEventListener('scroll', () => {

    const {scrollTop, clientHeight} = document.documentElement;

    // console.log(scrollTop, clientHeight);
    // console.log(slidingNewsletter.getBoundingClientRect());
    

    const topElementToTopViewport = slidingNewsletter.getBoundingClientRect().top;


    if(scrollTop > (scrollTop + topElementToTopViewport).toFixed() - clientHeight * 0.70) {
        slidingNewsletter.classList.add('active');

    }
})