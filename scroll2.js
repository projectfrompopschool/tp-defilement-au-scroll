// https://developer.mozilla.org/fr/docs/Web/API/Intersection_Observer_API//

const ratio = .1

const options = {
    root: null,
    rootMargin: '0px',
    threshold: ratio
}

const handleIntersect = function (entries, observer) {
    entries.forEach(function (entry) {
        if (entry.intersectionRatio > ratio) {
            entry.target.classList.add('reveal-visible')
            // console.log('visible')
            observer.unobserve(entry.target)
        }
        // else{
        //     console.log('invisible')
        // }
        // console.log(entry.intersectionRatio)
    })
    // console.log('handleIntersect')
}

const observer = new IntersectionObserver(handleIntersect, options)
// observer.observe(document.querySelector('.reveal'))
document.querySelectorAll('.reveal').forEach(function (r) {
    observer.observe(r)
})